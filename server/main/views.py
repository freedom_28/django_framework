from django.template import Template, Context
from django.template.loader import get_template, render_to_string
from django.http import HttpResponse
from django.shortcuts import render


def index(request):
    # template = Template(
    #     'Hello {{name}}'
    # )
    # context = Context({
    #     'name' : 'Igor'
    # })
    template = get_template(
        'main/index.html'
    )
    context = {
        'name' : 'Igor'
    }
    return HttpResponse(
        template.render(context)
    )
    # return render(
    #   request,
    #   'main/index.html'
    # )
    

def about(request):
    return render(
        request,
        'main/about.html'
    )

def contacts(request):
    return render(
        request,
        'main/contacts.html'
    )